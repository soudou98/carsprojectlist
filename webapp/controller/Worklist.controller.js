sap.ui.define([
    "./BaseController",
    "sap/ui/model/json/JSONModel",
    "../model/formatter",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator"
], function (BaseController, JSONModel, formatter, Filter, FilterOperator) {
    "use strict";

    return BaseController.extend("carsprojectlist.controller.Worklist", {

        formatter: formatter,

        /* =========================================================== */
        /* lifecycle methods                                           */
        /* =========================================================== */

        /**
         * Called when the worklist controller is instantiated.
         * @public
         */
        onInit : function () {
            var oViewModel;

            // keeps the search state
            this._aTableSearchState = [];

            // Model used to manipulate control states
            oViewModel = new JSONModel({
                worklistTableTitle : this.getResourceBundle().getText("worklistTableTitle"),
                shareSendEmailSubject: this.getResourceBundle().getText("shareSendEmailWorklistSubject"),
                shareSendEmailMessage: this.getResourceBundle().getText("shareSendEmailWorklistMessage", [location.href]),
                tableNoDataText : this.getResourceBundle().getText("tableNoDataText")
            });
            this.setModel(oViewModel, "worklistView");

        },

        /* =========================================================== */
        /* event handlers                                              */
        /* =========================================================== */

        /**
         * Triggered by the table's 'updateFinished' event: after new table
         * data is available, this handler method updates the table counter.
         * This should only happen if the update was successful, which is
         * why this handler is attached to 'updateFinished' and not to the
         * table's list binding's 'dataReceived' method.
         * @param {sap.ui.base.Event} oEvent the update finished event
         * @public
         */
        onUpdateFinished : function (oEvent) {
            // update the worklist's object counter after the table update
            var sTitle,
                oTable = oEvent.getSource(),
                iTotalItems = oEvent.getParameter("total");
            // only update the counter if the length is final and
            // the table is not empty
            if (iTotalItems && oTable.getBinding("items").isLengthFinal()) {
                sTitle = this.getResourceBundle().getText("worklistTableTitleCount", [iTotalItems]);
            } else {
                sTitle = this.getResourceBundle().getText("worklistTableTitle");
            }
            this.getModel("worklistView").setProperty("/worklistTableTitle", sTitle);
        },

        /**
         * Event handler when a table item gets pressed
         * @param {sap.ui.base.Event} oEvent the table selectionChange event
         * @public
         */
        onPress : function (oEvent) {
            // The source is the list item that got pressed
            this._showObject(oEvent.getSource());
        },

        onCreate : function (oEvent) {
            var that = this;
            var oModel = this.getOwnerComponent().getModel();
            oModel.setUseBatch(false);
                var omatricule = oEvent.getSource().getParent().getParent().getCells()[0].getValue();
                var omarque    = oEvent.getSource().getParent().getParent().getCells()[1].getValue();
                var omodele    = oEvent.getSource().getParent().getParent().getCells()[2].getValue();
                var opf        = oEvent.getSource().getParent().getParent().getCells()[3].getValue();

                var oEntry = {
                    Matricule: omatricule,
                    Marque: omarque,
                    Modele: omodele,
                    Pf: opf
                };

                oModel.create("/ZCARS_YSO_DATASet",oEntry,{success: function(odata){
                },error: function(oError){
                    console.log(oError);
                }
                })

                console.log(oEntry);

        },

        /**
         * Event handler for navigating back.
         * Navigate back in the browser history
         * @public
         */
        onNavBack : function() {
            // eslint-disable-next-line sap-no-history-manipulation
            history.go(-1);
        },

        onEdit : function(oEvent) {
            var that = this;
            var oModel = this.getOwnerComponent().getModel();
            oModel.setUseBatch(false);
            if(oEvent.getSource().getText() === "Edit"){
                oEvent.getSource().setText("Submit");
                oEvent.getSource().getParent().getParent().getCells()[3].setEditable(true);
                oEvent.getSource().getParent().getParent().getCells()[2].setEditable(true);
                oEvent.getSource().getParent().getParent().getCells()[1].setEditable(true);

            }else{
                oEvent.getSource().setText("Edit");
                oEvent.getSource().getParent().getParent().getCells()[3].setEditable(false);
                oEvent.getSource().getParent().getParent().getCells()[2].setEditable(false);
                oEvent.getSource().getParent().getParent().getCells()[1].setEditable(false);
                var omatricule = oEvent.getSource().getParent().getParent().getCells()[0].mProperties.title;
                var omarque = oEvent.getSource().getParent().getParent().getCells()[1].getValue();
                var omodele = oEvent.getSource().getParent().getParent().getCells()[2].getValue();
                var opf = oEvent.getSource().getParent().getParent().getCells()[3].getValue();

                var oEntry = {
                    Marque: omarque,
                    Modele: omodele,
                    Pf: opf
                };

                oModel.update("/ZCARS_YSO_DATASet('"+ omatricule +"')",oEntry,{success: function(odata){
                },error: function(oError){
                    console.log(oError);
                }
                })

                const car = {
                    omatricule: omatricule,
                    omarque: omarque,
                    omodele: omodele,
                    opf: opf
                } 
                console.log(car);


                /*console.log(" "+ omarque +" "+ omodele +" "+ opf);
                oModel.update("/ZCARS_YSO_DATASet("+omatricule+")",{Rating:oInput},{
                    success: function(odata){
                        //that.onReadAll();
                    }, error: function(oError){
                        console.log(oError);
                    }
                }); */

            }
            //console.log(oModel);
        },

        onDelete : function(oEvent){
            var that = this;
            var oModel = this.getOwnerComponent().getModel();
            oModel.setUseBatch(false);
            var omatricule = oEvent.getSource().getParent().getParent().getCells()[0].mProperties.title;
            oModel.remove("/ZCARS_YSO_DATASet('"+ omatricule +"')",{success: function(odata){
            },error: function(oError){
                console.log(oError);
            }
            })
        },


        onSearch : function (oEvent) {
            if (oEvent.getParameters().refreshButtonPressed) {
                // Search field's 'refresh' button has been pressed.
                // This is visible if you select any main list item.
                // In this case no new search is triggered, we only
                // refresh the list binding.
                this.onRefresh();
            } else {
                var aTableSearchState = [];
                var sQuery = oEvent.getParameter("query");

                if (sQuery && sQuery.length > 0) {
                    aTableSearchState = [new Filter("Matricule", FilterOperator.Contains, sQuery)];
                }
                this._applySearch(aTableSearchState);
            }

        },

        /**
         * Event handler for refresh event. Keeps filter, sort
         * and group settings and refreshes the list binding.
         * @public
         */
        onRefresh : function () {
            var oTable = this.byId("table");
            oTable.getBinding("items").refresh();
        },

        /* =========================================================== */
        /* internal methods                                            */
        /* =========================================================== */

        /**
         * Shows the selected item on the object page
         * @param {sap.m.ObjectListItem} oItem selected Item
         * @private
         */
        _showObject : function (oItem) {
            this.getRouter().navTo("object", {
                objectId: oItem.getBindingContext().getPath().substring("/ZCARS_YSO_DATASet".length)
            });
        },

        /**
         * Internal helper method to apply both filter and search state together on the list binding
         * @param {sap.ui.model.Filter[]} aTableSearchState An array of filters for the search
         * @private
         */
        _applySearch: function(aTableSearchState) {
            var oTable = this.byId("table"),
                oViewModel = this.getModel("worklistView");
            oTable.getBinding("items").filter(aTableSearchState, "Application");
            // changes the noDataText of the list in case there are no filter results
            if (aTableSearchState.length !== 0) {
                oViewModel.setProperty("/tableNoDataText", this.getResourceBundle().getText("worklistNoDataWithSearchText"));
            }
        }

    });
});
