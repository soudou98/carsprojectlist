sap.ui.define([
    "./BaseController",
    "sap/ui/model/json/JSONModel",
    "../model/formatter",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator"
],
    function ([BaseController,JSONModel, formatter, Filter, FilterOperator]) {
        "use strict";

        return BaseController.extend("sap.btp.sapui5.controller.Create", {

            onInit : function () {
                var oRouter = sap.ui.core.UIComponent.getRouterFor();
                oRouter.getRoute("create");
            },

        });
    });