/* global QUnit */

sap.ui.require([
	"carsprojectlist/test/integration/AllJourneys"
], function() {
	QUnit.config.autostart = false;
	QUnit.start();
});