## Application Details
|               |
| ------------- |
|**Generation Date and Time**<br>Tue Feb 14 2023 16:41:05 GMT+0100 (GMT+01:00)|
|**App Generator**<br>@sap/generator-fiori-freestyle|
|**App Generator Version**<br>1.8.6|
|**Generation Platform**<br>CLI|
|**Template Used**<br>1worklist|
|**Service Type**<br>SAP System (ABAP On Premise)|
|**Service URL**<br>http://10.104.12.91:8000//sap/opu/odata/sap/ZCARS_YSO_ODATA_SRV
|**Module Name**<br>carsprojectlist|
|**Application Title**<br>cars project list|
|**Namespace**<br>|
|**UI5 Theme**<br>sap_horizon|
|**UI5 Version**<br>1.110.1|
|**Enable Code Assist Libraries**<br>False|
|**Enable TypeScript**<br>False|
|**Add Eslint configuration**<br>False|
|**Object collection**<br>ZCARS_YSO_DATASet|
|**Object collection key**<br>Matricule|
|**Object ID**<br>Matricule|
|**Object unit of measure**<br>Pf|

## carsprojectlist

A Fiori application.

### Starting the generated app

-   This app has been generated using the SAP Fiori tools - App Generator, as part of the SAP Fiori tools suite.  In order to launch the generated app, simply run the following from the generated app root folder:

```
    npm start
```

- It is also possible to run the application using mock data that reflects the OData Service URL supplied during application generation.  In order to run the application with Mock Data, run the following from the generated app root folder:

```
    npm run start-mock
```

#### Pre-requisites:

1. Active NodeJS LTS (Long Term Support) version and associated supported NPM version.  (See https://nodejs.org)


